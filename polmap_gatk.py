#!/usr/bin/python3
"""
Main pipeline script

To call it:
    polmap_gatk.py {reference.fa} {wt_sample1.fq} {wt_sample2.fq} {mutant_sample1.fq} {mutant_sample2.fq} {project_folder}

With:
    reference.fa:       path to a FASTA file to use as a reference
    wt_sample.fq:       path to one of the two FASTQ files for the Wild-Type sample, ending in .fq or .fq.gz
    mutant_sample.fq:   path to one of the two FASTQ files for the mutant sample, ending in .fq or .fq.gz
    project_folder:     path to an empty folder where you want your results
"""

# import statements
from sys import argv
import subprocess
import os
import shlex


# function definitions
def run_assembly(fq1, fq2, ref_fa, work_dir="./"):
    """Runs two FASTQ files through bwa mem and processes the resulting SAM file to be ready to use

    :param fq1:     first .fq file
    :param fq2:     second .fq file
    :param ref_fa:  the reference genome in fasta format
    :param work_dir: the directory where you want the BAM file
    :return:
    """
    sample_name = fq1.rpartition("_")[0].rpartition("/")[-1]
    sam_fn = work_dir + sample_name + ".sam"
    bam_fn = work_dir + sample_name + ".bam"
    dedup_fn = work_dir + sample_name + "_dedup.bam"
    stats_fn = work_dir + sample_name + "_dupstats.txt"

    cmd_bwamem = f'bwa mem -M -t 8 -R "@RG\\tID:{sample_name}\\tSM:{sample_name}" -o {sam_fn} {ref_fa} {fq1} {fq2}'
    cmd_samtools = f'samtools sort -@ 8 -o {bam_fn} {sam_fn}'
    cmd_index = 'samtools index {}'
    cmd_markduplicates = f'../gvcf_trial-main/gatk-4.2.2.0/gatk MarkDuplicates -I {bam_fn} -O {dedup_fn} ' \
                         f'-M {stats_fn} --ASSUME_SORT_ORDER coordinate --REMOVE_DUPLICATES true'

    if not os.path.exists(dedup_fn):
        if not os.path.exists(bam_fn):
            if not os.path.exists(sam_fn):
                # align with bwa mem
                subprocess.check_call(cmd_bwamem, shell=True)

            # sort and convert to bam with samtools sort
            subprocess.check_call(shlex.split(cmd_samtools))
            subprocess.check_call(shlex.split(cmd_index.format(bam_fn)))
            # subprocess.check_call(f"rm {sam_fn}")

        # remove duplicates with picard
        subprocess.check_call(cmd_markduplicates, shell=True)
        subprocess.check_call(shlex.split(cmd_index.format(dedup_fn)))
        # subprocess.check_call(f"rm {bam_fn}")

    return dedup_fn


def call_variants(bam_list, ref_fa, work_dir="./", project="my_project"):
    """takes a list of bam files and performs a joint variant call according to the GATK short SNV best practices

    :param bam_list:
    :param ref_fa:
    :param work_dir:
    :param project:
    :return:
    """
    out_vcf = work_dir + project + ".vcf.gz"
    cohort_fn = work_dir + project + "_cohort.vcf.gz"
    gvcfs = []

    cmd_hapcaller = '../gvcf_trial-main/gatk-4.2.2.0/gatk --java-options "-Xmx20g" HaplotypeCaller -ERC GVCF ' \
                    '-R {} -I {} -O {}'
    cmd_combine = f'../gvcf_trial-main/gatk-4.2.2.0/gatk --java-options "-Xmx20g" CombineGVCFs ' \
                  f'-R {ref_fa} -O {cohort_fn} '
    cmd_genotype = f'../gvcf_trial-main/gatk-4.2.2.0/gatk --java-options "-Xmx20g" GenotypeGVCFs ' \
                   f'-R {ref_fa} -V {cohort_fn} -O {out_vcf}'

    if not os.path.exists(out_vcf):
        if not os.path.exists(cohort_fn):
            # run HaplotypeCaller on each Bam file
            for bam_fn in bam_list:
                gvcf_fn = bam_fn.rpartition(".")[0] + ".vcf.gz"
                if not os.path.exists(gvcf_fn):
                    subprocess.check_call(shlex.split(cmd_hapcaller.format(ref_fa, bam_fn, gvcf_fn)))
                gvcfs.append(gvcf_fn)

            # add all created gvcf files to the combine command
            for file in gvcfs:
                cmd_combine += f"-V {file} "

            # run CombineGVCFs
            subprocess.check_call(shlex.split(cmd_combine))

        # run GenotypeGVCFs
        subprocess.check_call(shlex.split(cmd_genotype))

    return out_vcf


def annotate_snpeff(in_vcf):
    ann_vcf = in_vcf.rpartition(".")[0] + ".snpEff.vcf"
    stats_fn = ann_vcf.rpartition(".")[0] + ".stats.html"
    cmd = f"java -jar snpEff.jar -ud 0 -canon -stats {stats_fn} Arabidopsis_thaliana {in_vcf} > {ann_vcf}"

    if not os.path.exists(ann_vcf):
        subprocess.check_call(cmd, shell=True)
    return ann_vcf


def make_table(in_vcf):
    out_fn = in_vcf.rpartition(".")[0] + ".genetable.txt"
    sample_list = []

    for line in open(in_vcf):
        line = line.strip()
        if line.startswith("#CHROM"):
            samples = line.partition("FORMAT\t")[-1]
            sample_list = samples.split("\t")
            break

    cmd = f'java -jar SnpSift.jar ExtractFields -s "," -e "." {in_vcf} "CHROM" "POS" "REF" "ALT" "TYPE" "MQMR" "MQM" ' \
          f'"ANN[*].ALLELE" "ANN[*].EFFECT" "ANN[*].IMPACT" "ANN[*].GENE" "ANN[*].GENEID" "ANN[*].BIOTYPE" ' \
          f'"ANN[*].HGVS_C" "ANN[*].HGVS_P" '
    cmd_genfield = '"GEN[{0}].GT" "GEN[{0}].GQ" "GEN[{0}].AD" "GEN[{0}].DP" '

    for sample in sample_list:
        cmd += cmd_genfield.format(sample)

    cmd += f'> {out_fn}'

    if not os.path.exists(out_fn):
        subprocess.check_call(cmd, shell=True)

    return out_fn


# main code
if __name__ == "__main__":
    # parse input
    ref, wt_fq1, wt_fq2, mt_fq1, mt_fq2, out_dir = argv[1:]
    if not out_dir.endswith("/"):
        out_dir += "/"

    project_name = wt_fq1.rpartition("/")[-1].rpartition("w")[0]

    # index reference
    if not os.path.exists(ref + ".pac"):
        subprocess.check_call(shlex.split(f"bwa index {ref}"))
    
    if not os.path.exists(ref + ".fai"):
        subprocess.check_call(shlex.split(f"samtools faidx {ref}"))

    if not os.path.exists(ref.rpartition(".fa")[0] + ".dict"):
        subprocess.check_call(shlex.split(f"../gvcf_trial-main/gatk-4.2.2.0/gatk CreateSequenceDictionary -R {ref}"))

    # run assembly on each sample
    wt_bam = run_assembly(wt_fq1, wt_fq2, ref, work_dir=out_dir)
    mt_bam = run_assembly(mt_fq1, mt_fq2, ref, work_dir=out_dir)

    # Call variants with GATK HaplotypeCaller in GVCF mode
    vcf = call_variants([wt_bam, mt_bam], ref, work_dir=out_dir, project=project_name)

    # annotate with snpEff
    vcf_ann = annotate_snpeff(vcf)

    # add vartype

    # convert vcf file to a workable table
    genetable = make_table(vcf_ann)