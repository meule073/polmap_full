#!/usr/bin/python3
"""
Main pipeline script

To call it:
    python3 polmap_freebayes.py {bam_folder} {ref_fasta}

With:
    bam_folder: a folder containing only the .bam files to be analysed, and their respective .bai files
    ref_fasta: the reference genome in fasta format (ending on .fa)
"""
# import statements
from sys import argv
import subprocess
import os


# function definitions
def write_bam_list(dir_name):
    """Writes a file with all bam files in a given directory

    :param dir_name: str, the name of the project directory
    :return: the name of the created file
    """
    if not dir_name.endswith("/"):
        dir_name = dir_name + "/"

    sample_name = dir_name.split("/")[-2]
    out_fn = dir_name + sample_name + ".fofn"
    out_file = open(out_fn, "w")

    dir_contents = os.listdir(dir_name)
    for fn in dir_contents:
        if fn.endswith(".bam"):
            out_file.write(dir_name + fn + "\n")

    out_file.close()

    return out_fn


def write_regions_by_bai(bamlist, reffasta):
    """Creates a file with regions of the genome divided by the amount of data in the bam files

    :param bamlist: str, the filename of the file containing the names of all the .bam files to be analysed
    :param reffasta: str, the filename of the reference genome in fasta format (ending on .fa)
    :return: str, the name of the created .regions file
    """
    out_fn = bamlist.rpartition('.')[0] + ".regions"
    cmd = f"python3 ./freebayes/scripts/split_ref_by_bai_datasize.py -L {bamlist} -r {reffasta} > {out_fn}"

    subprocess.check_call(cmd, shell=True)

    return out_fn


def write_regions_by_fai(reffasta, dir_name, regionsize=100000000):
    """Calls fasta_generate_regions.py to create a regions file for freebayes-parallel

    :param reffasta: str, the filename of the reference genome in fasta format (ending on .fa)
    :param dir_name: dir_name: str, the name of the project directory
    :param regionsize: int, the size of each created region
    :return: str, the name of the created .regions file
    """
    sample_name = dir_name.split("/")[-2]
    out_fn = dir_name + sample_name + ".regions"

    cmd = f"python3 ./freebayes/scripts/fasta_generate_regions.py {reffasta}.fai {regionsize} > {out_fn}"
    subprocess.check_call(cmd, shell=True)

    return out_fn


def run_freebayes_parallel(regions, reffasta, bamlist, ncpus=8, ploidy=100, best_n_alleles=4):
    """Runs the script freebayes-parallel

    :param regions: str, the filename of the regions file, which defines which regions should be ran in parallel
    :param reffasta: str, the filename of the reference genome in fasta format (ending on .fa)
    :param bamlist: str, the filename of the file containing the names of all the .bam files to be analysed
    :param ncpus: int, number of cpu cores to run FreeBayes on in parallel (default 8)
    :param best_n_alleles: int, maximum number of alleles to call at a locus (default 4)
    :param ploidy: int, ploidy of the sample equal to {pool size} * {ploidy of organism}
    :return: str, name of the vcf file created by freebayes
    """
    out_fn = bamlist.rpartition('.')[0] + ".freebayes.vcf"
    cmd = f"bash ./freebayes/scripts/freebayes-parallel {regions} {ncpus} " \
          f"--bam-list {bamlist} " \
          f"--fasta-reference {reffasta} " \
          f"--pooled-discrete --ploidy {ploidy} " \
          f"--use-best-n-alleles {best_n_alleles} " \
          f"--genotype-qualities " \
          f"> {out_fn}"
    # remove --genotype-qualities if the script crashes

    if not os.path.exists(out_fn):
        subprocess.check_call(cmd, shell=True)

    return out_fn


def freebayes_parallel_picard(regions, reffasta, bamlist, ncpus=8, ploidy=100, best_n_alleles=4):
    """Runs freebayes in parallel without the freebayes-parallel script with tools already installed on wasabi

    :param regions: str, the filename of the regions file, which defines which regions should be ran in parallel
    :param reffasta: str, the filename of the reference genome in fasta format (ending on .fa)
    :param bamlist: str, the filename of the file containing the names of all the .bam files to be analysed
    :param ncpus: int, number of cpu cores to run FreeBayes on in parallel (default 8)
    :param best_n_alleles: int, maximum number of alleles to call at a locus (default 4)
    :param ploidy: int, ploidy of the sample equal to {pool size} * {ploidy of organism}
    :return: str, name of the vcf file created by freebayes
    """
    out_fn = bamlist.rpartition('.')[0] + ".freebayes.vcf"
    unsorted_fn = out_fn.rpartition(".")[0] + "_unsorted.vcf"

    cmd = f"cat {regions} " \
          f"| parallel -k -j {ncpus} freebayes " \
          f"--bam-list {bamlist} " \
          f"--fasta-reference {reffasta} " \
          f"--pooled-discrete --ploidy {ploidy} " \
          f"--use-best-n-alleles {best_n_alleles} " \
          f"--region {{}} " \
          f"| ./freebayes/scripts/vcffirstheader " \
          f"> {unsorted_fn}"
    cmd_sort = f'./gatk-4.2.2.0/gatk --java-options "-Xmx20g" SortVcf -I {unsorted_fn} -O {out_fn}'

    if not os.path.exists(out_fn):
        subprocess.check_call(cmd, shell=True)
        subprocess.check_call(cmd_sort, shell=True)

    return out_fn


def annotate_snpeff(in_vcf):
    ann_vcf = in_vcf.rpartition(".")[0] + ".snpEff.vcf"
    stats_fn = ann_vcf.rpartition(".")[0] + ".stats.html"
    cmd = f"java -jar snpEff.jar -ud 0 -canon -stats {stats_fn} Arabidopsis_thaliana {in_vcf} > {ann_vcf}"

    if not os.path.exists(ann_vcf):
        subprocess.check_call(cmd, shell=True)
    return ann_vcf


def make_table(in_vcf):
    out_fn = in_vcf.rpartition(".")[0] + ".genetable.txt"
    sample_list = []

    for line in open(in_vcf):
        line = line.strip()
        if line.startswith("#CHROM"):
            samples = line.partition("FORMAT\t")[-1]
            sample_list = samples.split("\t")
            break

    cmd = f'java -jar SnpSift.jar ExtractFields -s "," -e "." {in_vcf} "CHROM" "POS" "REF" "ALT" "TYPE" "MQMR" "MQM" ' \
          f'"ANN[*].ALLELE" "ANN[*].EFFECT" "ANN[*].IMPACT" "ANN[*].GENE" "ANN[*].GENEID" "ANN[*].BIOTYPE" ' \
          f'"ANN[*].HGVS_C" "ANN[*].HGVS_P" '
    cmd_genfield = '"GEN[{0}].GT" "GEN[{0}].GQ" "GEN[{0}].AD" "GEN[{0}].DP" '

    for sample in sample_list:
        cmd += cmd_genfield.format(sample)

    cmd += f'> {out_fn}'

    if not os.path.exists(out_fn):
        subprocess.check_call(cmd, shell=True)

    return out_fn


# main code
if __name__ == "__main__":
    # parse input
    bam_dir = argv[1]
    ref_fasta = argv[2]

    # generate bam-list file from the BAM_input directory
    bam_list = write_bam_list(bam_dir)

    # generate regions to use for freebayes-parallel.py
    region_file = write_regions_by_fai(ref_fasta, bam_dir)
    # region_file = write_regions_by_bai(bam_list, ref_fasta)

    # run freebayes-parallel.py
    vcf = run_freebayes_parallel(region_file, ref_fasta, bam_list)
    # vcf = freebayes_parallel_picard(region_file, ref_fasta, bam_list)

    # annotate with snpEff
    vcf_ann = annotate_snpeff(vcf)

    # convert vcf file to a workable table
    genetable = make_table(vcf_ann)

    # generate plots for determining filters

    # filter vcf output

    # annotate with snpEff (can we add GO terms?)

    # get interesting candidates with snpSift

    # generate files and plots for analysis
