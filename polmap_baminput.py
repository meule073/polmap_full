#!/usr/bin/python3
"""
Script to run the pipeline with 3 bam

To call it:
    polmap_gatk.py {reference.fa} {project_folder}

With:
    reference.fa:       path to a FASTA file to use as a reference
    wt_sample.fq:       path to one of the two FASTQ files for the Wild-Type sample, ending in .fq or .fq.gz
    mutant_sample.fq:   path to one of the two FASTQ files for the mutant sample, ending in .fq or .fq.gz
    project_folder:     path to an empty folder where you want your results
"""

# import statements
from sys import argv
import subprocess
import os
import shlex


# function definitions
def call_variants(bam_list, ref_fa, work_dir="./", project="my_project"):
    """takes a list of bam files and performs a joint variant call according to the GATK short SNV best practices

    :param bam_list:
    :param ref_fa:
    :param work_dir:
    :param project:
    :return:
    """
    out_gvcf = work_dir + project + ".vcf.gz"
    cohort_fn = work_dir + project + "_cohort.vcf.gz"
    gvcfs = []

    cmd_hapcaller = '../gvcf_trial-main/gatk-4.2.2.0/gatk --java-options "-Xmx20g" HaplotypeCaller -ERC GVCF ' \
                    '-R {} -I {} -O {}'
    cmd_combine = f'../gvcf_trial-main/gatk-4.2.2.0/gatk --java-options "-Xmx20g" CombineGVCFs ' \
                  f'-R {ref_fa} -O {cohort_fn} '
    cmd_genotype = f'../gvcf_trial-main/gatk-4.2.2.0/gatk --java-options "-Xmx20g" GenotypeGVCFs ' \
                   f'-R {ref_fa} -V {cohort_fn} -O {out_gvcf}'

    if not os.path.exists(out_gvcf):
        if not os.path.exists(cohort_fn):
            # run HaplotypeCaller on each Bam file
            for bam_fn in bam_list:
                gvcf_fn = bam_fn.rpartition(".")[0] + ".vcf.gz"
                if not os.path.exists(gvcf_fn):
                    subprocess.check_call(shlex.split(cmd_hapcaller.format(ref_fa, bam_fn, gvcf_fn)))
                gvcfs.append(gvcf_fn)

            # add all created gvcf files to the combine command
            for file in gvcfs:
                cmd_combine += f"-V {file} "

            # run CombineGVCFs
            subprocess.check_call(shlex.split(cmd_combine))

        # run GenotypeGVCFs
        subprocess.check_call(shlex.split(cmd_genotype))

    return out_gvcf


# main code
if __name__ == "__main__":
    # parse input
    # ref, wt_bam, mt_bam, bg_bam, out_dir = argv[1:]
    ref = argv[1]
    out_dir = argv[2]
    bams = argv[3:]
    if not out_dir.endswith("/"):
        out_dir += "/"

    project_name = bams[0].rpartition("/")[-1].rpartition("w")[0]

    # index reference
    if not os.path.exists(ref + ".fai"):
        subprocess.check_call(shlex.split(f"samtools faidx {ref}"))

    if not os.path.exists(ref.rpartition(".fa")[0] + ".dict"):
        subprocess.check_call(shlex.split(f"../gvcf_trial-main/gatk-4.2.2.0/gatk CreateSequenceDictionary -R {ref}"))

    # Call variants with GATK HaplotypeCaller in GVCF mode
    gvcf = call_variants(bams, ref, work_dir=out_dir, project=project_name)

